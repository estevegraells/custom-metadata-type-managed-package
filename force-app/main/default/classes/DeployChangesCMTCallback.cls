/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Callback for Metadata Deployment
 */ 
public class DeployChangesCMTCallback implements Metadata.DeployCallback {
    
    public void handleResult(Metadata.DeployResult result,
                             Metadata.DeployCallbackContext context) {
        
        if (result.status == Metadata.DeployStatus.Succeeded) {

            System.debug('-ege- Callouts made at end: ' + ege.RiskServiceProxyAccess.getTotalPayableCallouts());

        } else {

           System.Debug('-ege- Deployment failed: ' + result.status + ' with error message: ' + result.errorMessage);

        } 

    }
}
