/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Proxy Class for Risk Level Service
*/ 
 
global with sharing class RiskCreditCntController {

    public static RiskCredit__mdt riskServiceConn; 

    public static String url = '';
    public static String username = '';
    public static String password = '';

    global String riskLevelObtained {get;set;}
    global String customerId {get;set;}
    public String requestLabel;

    global RiskCreditCntController() {

        System.debug('-ege- RiskServiceProxyAccess constructor');

    }

    /* 
    * @description Gets credentials and enpoint from CMT, calls the service and update usage on CMT accordingly
    * @param ident: dni/nif/nie identifier to check risk against to
    */ 
    global Object getRiskValue(){

        return getRiskValueInternal();
    }

    public Object getRiskValueInternal(){

        String creditRiskLevel = '';

        //Get Credentials from Custom Metadata Type
        riskServiceConn = [SELECT 
                ege__Target_URL__c,
                ege__Username__c,
                ege__Password__c,
                ege__Callouts_Made__c,
                ege__Last_callout_date__c,
                DeveloperName,
                Label,
                QualifiedApiName
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = false 
                LIMIT 1];

        System.debug('-ege- Connection Details obtained: ' + riskServiceConn);

        //Create a Continuation
        Continuation c = new Continuation (20);
        c.ContinuationMethod = 'callbackContinuation';

        //Call service and add it to the continuation
        if (riskServiceConn != null){

            Http http = new Http();
            HttpRequest req = new HttpRequest();

            req.setMethod('GET');
            req.setEndpoint(riskServiceConn.Target_URL__c + '?ident=' + customerId);
          
            this.requestLabel = c.addHttpRequest(req);
        }

        return c;

    }

    global Object callbackContinuation(){

        // Get the response by using the unique label
        HttpResponse response = Continuation.getResponse(requestLabel);

        riskLevelObtained = response.getBody();

        // Set the result variable that is displayed on the Visualforce page
        if (response != null){
            riskLevelObtained = response.getBody();

            if ( riskLevelObtained != null ){
                updateUsageOnCMT();
            }
        }else {
            System.debug( '-ege- Response from Continuation is null' );
        }
                
        // Return null to re-render the original Visualforce page
        return null;

    }

    
    /*
    * @description Updates Usage on the CMT, it requires a Metadata deployment
    */
    global boolean updateUsageOnCMT(){

        System.debug('-ege- Updating CMT ');

        if ( riskServiceConn == null ){
            //Get Credentials from Custom Metadata Type
            riskServiceConn = [SELECT 
                ege__Target_URL__c, 
                ege__Username__c, 
                ege__Password__c, 
                ege__Callouts_Made__c,
                ege__Last_callout_date__c, 
                label, 
                QualifiedApiName
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = false 
                LIMIT 1];
        }

        // Create a new CustomMedata Instance: both fullName and label are mandatory
        //Don't forget namespace, otherwise you'll get errors on the suscriber ORG spending hours like me!!
        Metadata.CustomMetadata customMetadata =  new Metadata.CustomMetadata();
        customMetadata.fullName = 'ege__RiskCredit__mdt.' + riskServiceConn.QualifiedApiName;
        customMetadata.label = riskServiceConn.label; 
        
        // Update total calls made counter 
        Metadata.CustomMetadataValue calloutsMadeField = new Metadata.CustomMetadataValue();
        calloutsMadeField.field = 'ege__Callouts_Made__c';
        calloutsMadeField.value = riskServiceConn.ege__Callouts_Made__c + 1;
        customMetadata.values.add(calloutsMadeField);

        // Update total calls made counter 
        Metadata.CustomMetadataValue lastCalloutDateField = new Metadata.CustomMetadataValue();
        lastCalloutDateField.field = 'ege__Last_callout_date__c';
        lastCalloutDateField.value = date.today();
        customMetadata.values.add(lastCalloutDateField);

        //Create the container to deploy
        Metadata.DeployContainer container = new Metadata.DeployContainer();
        container.addMetadata(customMetadata);

        //It is not allowed to deploy during a test running
        if ( !Test.isRunningTest() ){

            //DeployChangesCMTCallback callback = new ege.DeployChangesCMTCallback();
            //The callback for the Deployment is not alowed when executing the Continuation Callback
            Metadata.Operations.enqueueDeployment(container, null);  
        }
        
        return true;
    }
}
