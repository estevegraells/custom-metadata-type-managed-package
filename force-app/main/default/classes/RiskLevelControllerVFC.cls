/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Controller for the VF Component, all the service and offers variable bindings
 */ 
 
 public with sharing class RiskLevelControllerVFC {

    private String customerIDCtrl;
    private String riskLevel;
    
    public String getCustomerIDCtrl(){
        return customerIDCtrl;
    }
    
    public void setCustomerIDCtrl(String id){
        customerIDCtrl = id;
    }

    public String getRiskLevel(){

        String riskObtained = ege.RiskServiceProxyAccess.getRiskValue(customerIDCtrl);
        return riskObtained;
    }
}
