/*
 * @author Esteve Graells
 * @date: Jan 2019
 * @description: Service Callout management class
 */

public with sharing class GetCreditLevelCallout {

    private static final Integer CALLOUT_TIMEOUT = 120000; //millis
    
    private static String url = '';
    private static String username = '';
    private static String password = '';

    /*
    * @description Class Constructor
    * @param URL to access the service
    * @param username for service requests
    * @param password for service requests
    */
    public GetCreditLevelCallout(String targetUrl, String usn, String pwd){

        url         = targetUrl;
        username    = usn;
        password    = pwd;

    }

    /*
    * @description Make a syncrhronous Callout to the external service
    * @return raw result from web service, documentation on this should be provided to user
    */
    
    public static String makeCallout(String id){ 

        Http http = new Http();
        HttpResponse res; 
        HttpRequest req = new HttpRequest();

        //Customer ident is passed as param in the call - just for demo purposes
        //oAuth dialog should be used instead in real scenarios with Named Credentials
        req.setEndpoint(url + '?ident=' + id);
        req.setMethod('GET');
        req.setTimeout(CALLOUT_TIMEOUT);
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        try {
            
            res = http.send(req);
        
        } catch (Exception e) {
            
            System.debug('-ege- Callout made but error ocurred, details: ' + e);
        
        } finally {

            if ( res != null){

                System.debug('-ege- Web Service obtained body: ' + res.getBody());
                return res.getBody();

            }else{

                System.debug('-ege- Error, result from Remote Service is null');
            }
        }

        return null;
        
    }
}