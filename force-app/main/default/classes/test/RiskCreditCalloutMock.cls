/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Web Service Mock class, more extensive testing should be done
 */ 
 
 @isTest
 public class RiskCreditCalloutMock implements HttpCalloutMock{

    public HttpResponse respond(HttpRequest req) {

        System.assert(req.getEndpoint().contains('https://gentle-reef-50190.herokuapp.com/pro/get'), 'Incorrect Endpoint');
        System.assertEquals('GET', req.getMethod(), 'Incorrect Http Method');
        System.assert(req.getHeader('Authorization') != null, 'No Authorization header found');

        String identReceived = req.getEndpoint().substringAfter('ident=').left(10);
        System.debug('-ege- Identifier received:' + identReceived);
        
        // Create a fake response
        HttpResponse res = new HttpResponse();

        switch on identReceived.right(1) {
           when  'H'{
               res.setBody('high');
           }
           when  'L'{
               res.setBody('low');
           }
           when  'M'{
               res.setBody('medium');
           }
           when else {
               res.setBody('none');
           }
        }

        res.setStatusCode(200);

        return res;

    }
}