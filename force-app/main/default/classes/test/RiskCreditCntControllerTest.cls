/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Testing class, more extensive testing should be done
 */ 

@isTest
public with sharing class RiskCreditCntControllerTest {

    @isTest static void test1(){

        RiskCreditCntController cntController = new RiskCreditCntController ();

        Object c1 = cntController.getRiskValue();
        System.assertNotEquals(null, c1);

        Object c2 = cntController.getRiskValueInternal();
        System.assertNotEquals(null, c2);

        boolean updateCMT = cntController.updateUsageOnCMT();
        System.assertEquals(true, updateCMT);
    }
}
