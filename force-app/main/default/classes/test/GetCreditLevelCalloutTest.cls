/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Testing class, more extensive testing should be done
 */ 
 
@isTest
public with sharing class GetCreditLevelCalloutTest {
    
    public GetCreditLevelCalloutTest() {

        System.debug('-ege- Entering Callout test');
    }

    @isTest static void testInSandbox(){

        //Set mock class for callout
        Test.setMock(HttpCalloutMock.class, new RiskCreditCalloutMock());

        String url = 'https://gentle-reef-50190.herokuapp.com/pro/get';
        String username = 'usr_secret';
        String password = 'pwd_secret';
        
        GetCreditLevelCallout service = new GetCreditLevelCallout(url, username, password);

        String ident = '123456789L';
        String creditRiskLevel = GetCreditLevelCallout.makeCallout(ident);
        System.assertEquals('low', creditRiskLevel);

        ident = '123456789H';
        creditRiskLevel = GetCreditLevelCallout.makeCallout(ident);
        System.assertEquals('high', creditRiskLevel);

        ident = '123456789M';
        creditRiskLevel = GetCreditLevelCallout.makeCallout(ident);
        System.assertEquals('medium', creditRiskLevel);
        
        ident = '';
        creditRiskLevel = GetCreditLevelCallout.makeCallout(ident);
        System.assertEquals('none', creditRiskLevel);
        
    }
}