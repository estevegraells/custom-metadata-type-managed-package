/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Testing class, more extensive testing should be done
 */ 

@isTest

public with sharing class RiskServiceProxyAccessTest {
    
    public RiskServiceProxyAccessTest() {

        System.debug('-ege- RiskServiceProxyAccessTest Constructor' );    

    }

    @isTest static void test1(){

        //Set mock class for callout
        Test.setMock(HttpCalloutMock.class, new RiskCreditCalloutMock());
        
        //Invoke Remote service
        String creditRiskLevel = RiskServiceProxyAccess.getRiskValue('123456789L');
        System.assertEquals('low', creditRiskLevel);

        Decimal calloutsMade = RiskServiceProxyAccess.getTotalPayableCallouts(); 
        System.assert(calloutsMade >= 0);

        Date lastDateUsed = RiskServiceProxyAccess.getLastDateServiceUsed();
        System.assert(lastDateUsed != null);

        System.debug('-ege- Client gets this result: ' + creditRiskLevel);

    }
}
