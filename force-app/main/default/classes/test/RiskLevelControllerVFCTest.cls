/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  Testing class, more extensive testing should be done
 */ 

@isTest
public with sharing class RiskLevelControllerVFCTest {
    
    public RiskLevelControllerVFCTest() {

    }

    @isTest static void test1(){

        RiskLevelControllerVFC c = new RiskLevelControllerVFC();
        
        String customerId = '123456L';
        c.setCustomerIDCtrl(customerId);

        System.AssertEquals(customerId, c.getCustomerIDCtrl());

        //Set mock class for callout
        Test.setMock(HttpCalloutMock.class, new RiskCreditCalloutMock());

        String riskLevel = c.getRiskLevel();

        System.assertEquals('low', riskLevel);
    }


}
