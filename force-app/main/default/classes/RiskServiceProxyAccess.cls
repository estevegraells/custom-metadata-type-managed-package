/*
 * @author Esteve Graells 
 * @date: Jan 2019  
 * @description:  This is the main logic class, acts as a Proxy Class for Risk Level Service.
 * It's the entry point for calls from suscriber Org.
*/ 
 
global with sharing class RiskServiceProxyAccess {

    public static RiskCredit__mdt riskServiceConn; 

    public static String url = '';
    public static String username = '';
    public static String password = '';

    public RiskServiceProxyAccess() {

        System.debug('-ege- RiskServiceProxyAccess constructor');

    }

    /* 
    * @description Gets credentials and enpoint from CMT, calls the service and update usage on CMT accordingly
    * @param ident: dni/nif/nie identifier to check risk against to
    */ 
    global static String getRiskValue(String customerId){

        String creditRiskLevel = '';

        //Get credentials depending on the instance we are (Sandbox/PRO) stored on the CMT
        if ( url == '' || username == '' || password == ''){

            if (! getInfoFromCMT() ){

                System.debug('Problem obtaining connection details.');
                return null;
        
            }
        }
        
        //Call the service
        creditRiskLevel = callRiskLevelService(customerId);

        //If we got results update counter    
        if ( creditRiskLevel != null ){

            updateUsageOnCMT();

         } else{
             
             System.debug('-ege- Error, result from Remote Service is null');

         }
        
        return creditRiskLevel;

    }


    /* 
    * @description Get Total of callouts made to production endpoint
    * @return Integer Total Count
    */ 
    global static Decimal getTotalPayableCallouts(){

        return [SELECT 
                ege__Callouts_Made__c
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = false 
                LIMIT 1].ege__Callouts_Made__c;

    }

    /* 
    * @description Get Last Date production service was used
    * @return Date Last date service was used
    */ 
    global static Date getLastDateServiceUsed(){

        return [SELECT 
                ege__Last_callout_date__c
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = false 
                LIMIT 1].ege__Last_callout_date__c;

    }

    /* 
    * @description Gets endpoint and credentials from Custom Metadata Type (CMT) 
    * for sandbox or production instance
    * @return Boolean Query to CMT was successfull
    */ 
    public static boolean getInfoFromCMT() {

        // Am I in a Sandbox? give credentials for the free service
        if (runningInASandbox()) {
       
            System.debug('-ege- Running in a Sandbox' );

            riskServiceConn = riskServiceConn = [SELECT 
                ege__Target_URL__c,
                ege__Username__c,
                ege__Password__c,
                ege__Callouts_Made__c,
                ege__Last_callout_date__c,
                DeveloperName,
                Label,
                QualifiedApiName
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = true 
                LIMIT 1];
            
        }else{

            // Otherwise, give access to the paid service
            System.debug('-ege- Running in Production' );
            
            riskServiceConn = [SELECT 
                ege__Target_URL__c,
                ege__Username__c,
                ege__Password__c,
                ege__Callouts_Made__c,
                ege__Last_callout_date__c,
                DeveloperName,
                Label,
                QualifiedApiName
                FROM RiskCredit__mdt 
                WHERE IsFreeService__c = false 
                LIMIT 1];
        }

        System.debug('-ege- Connection Details obtained: ' + riskServiceConn);

        if (riskServiceConn != null){

            url = riskServiceConn.Target_URL__c;
            username = riskServiceConn.Username__c;
            password = riskServiceConn.Password__c;

        }

        return (url != null && username  != null && password != null);
    }
    
    
    /* 
    * @description  Synchrnous calling the service with the information and identifier 
    * @return Full result from service
    */ 

    public static String callRiskLevelService(String customerId) {

        GetCreditLevelCallout service = new GetCreditLevelCallout(url, username, password);
        return GetCreditLevelCallout.makeCallout(customerId);
        
    }

    /*
    * @description Updates Usage on the CMT, it requires a Metadata deployment
    */
    global static void updateUsageOnCMT(){

        if ( riskServiceConn == null ){

            getInfoFromCMT();

        }

        System.debug('-ege- Updating CMT ');

        // Create a new CustomMedata Instance: both fullName and label are mandatory
        //Don't forget namespace, otherwise you'll get errors on the suscriber ORG spending hours like me!!
        Metadata.CustomMetadata customMetadata =  new Metadata.CustomMetadata();
        customMetadata.fullName = 'ege__RiskCredit__mdt.' + riskServiceConn.QualifiedApiName; //Not Developer's Name
        customMetadata.label = riskServiceConn.label; 

        System.debug ('-ege- El Custom Metadata es: ' + customMetadata);
        
        // Update total calls made counter 
        Metadata.CustomMetadataValue calloutsMadeField = new Metadata.CustomMetadataValue();
        calloutsMadeField.field = 'ege__Callouts_Made__c';
        calloutsMadeField.value = riskServiceConn.ege__Callouts_Made__c + 1;
        customMetadata.values.add(calloutsMadeField);

        // Update total calls made counter 
        Metadata.CustomMetadataValue lastCalloutDateField = new Metadata.CustomMetadataValue();
        lastCalloutDateField.field = 'ege__Last_callout_date__c';
        lastCalloutDateField.value = date.today();
        customMetadata.values.add(lastCalloutDateField);

        //Create the container to deploy
        Metadata.DeployContainer container = new Metadata.DeployContainer();
        container.addMetadata(customMetadata);

        //It is not allowed to enqueue and run on Test
        if ( !Test.isRunningTest() ){

            DeployChangesCMTCallback callback = new DeployChangesCMTCallback();
            Metadata.Operations.enqueueDeployment(container, callback); 
        }
    }

    /*
    * @description Provides true/false if the current instance is a sandbox
    * @returns boolean if the current instance is a Sandbox
    */
    public static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
    }
    
}
